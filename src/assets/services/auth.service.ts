import { Client } from '../interfaces/Client';
import { DataService } from './data.service';
import { EventEmitter, Injectable} from '@angular/core';
import { Router } from '@angular/router';
import { ClientSubmit } from 'src/app/login/login.component';


@Injectable({
  providedIn: 'root'
})
export class AuthService{

  private _clientAuthentication: boolean = false;

  get clientAuthentication(): any{
    return this._clientAuthentication;
  }

  set clientAuthentication(clientAuthentication: any){
    this._clientAuthentication = clientAuthentication;
  }

  authentication = new EventEmitter<boolean>();

  client: Client;

  constructor(private router: Router, _dataService: DataService) { 
    this.client = _dataService.client;
  }

  makeLogin(clientSubmit: ClientSubmit){
    
    if (clientSubmit.account == this.client.clientNumber && clientSubmit.password == this.client.clientPassword){
    this._clientAuthentication = true;
    this.router.navigate(['/extrato']);
    this.authentication.emit(true);
    }else{
      this._clientAuthentication = false;
      this.authentication.emit(false);
    }
  }

}

