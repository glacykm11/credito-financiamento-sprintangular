import { Injectable } from '@angular/core';

import { AccountContents } from '../interfaces/AccountContents';
import { Client } from '../interfaces/Client';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  constructor() { }

  /*Objeto definido por um par "chave": valor, Use " " para ser 
  compatível com o JavaScript Object Notation JSON*/

  contents: AccountContents[] = [{
    "contentName": "pet shop",
    "contentLocal": "compra no cartão",
    "contentDate": "01/05/2021",
    "contentValue": -200.989,
  },
  {
    "contentName": "salário",
    "contentLocal": "crédito em conta",
    "contentDate": "08/05/2021",
    "contentValue": 2100,
  },
  {
    "contentName": "restaurante",
    "contentLocal": "compra no cartão",
    "contentDate": "11/07/2021",
    "contentValue": -120.17,
  },
  {
    "contentName": "Loja de roupas",
    "contentLocal": "compra no cartão",
    "contentDate": "06/09/2021",
    "contentValue": -39.89,
  },
  {
    "contentName": "Transferência",
    "contentLocal": "depósito em conta",
    "contentDate": "01/12/2021",
    "contentValue": 500.98,
  }];

  
  client: Client = {
    clientName: 'Danilo Macedo',
    clientNumber: 123,
    clientPassword: '123',
    clientBalance: 1000.00,
    clientEmail: 'danilomacedo@proway.com',
    clientCPF: 12345678901,
    clientRG: 123456789123,
    clientAddress: {
      address: 'Rua proway, n° 123',
      complement: 'apt 123',
      city: 'Proway',
      state: 'BA',
      cep: 12345678,
    },
    clientMonthlyIncome: 1000.00

  };
}
