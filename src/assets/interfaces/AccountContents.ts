//accountContents define as propiedades do conteúdo do extrato
/*Interface é definida por um par "chave": tipo, use " " para ser 
compatível com o JavaScript Object Notation JSON*/
export interface AccountContents {
    "contentName": string, //o que é.
    "contentLocal": string, //onde ocorreu.
    "contentDate": string, //quando ocorreu.
    "contentValue": number, //valor (positivo = crédito, negativo = débito).
}