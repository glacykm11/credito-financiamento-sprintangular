export interface Client{
    clientName: string;
    clientNumber: number;
    clientPassword: string;
    clientBalance: number;
    clientEmail?: string;
    clientCPF?: number;
    clientRG?: number;
    clientAddress?: {
        address: string;
        complement: string;
        city: string;
        state: string;
        cep: number; 
    };
    clientMonthlyIncome?: number;
}