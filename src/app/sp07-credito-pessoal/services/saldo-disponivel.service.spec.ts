import { TestBed } from '@angular/core/testing';

import { SaldoDisponivelService } from './saldo-disponivel.service';

describe('SaldoDisponivelService', () => {
  let service: SaldoDisponivelService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SaldoDisponivelService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
