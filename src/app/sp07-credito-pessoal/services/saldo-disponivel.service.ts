import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

import { SaldoDisponivel } from '../interfaces/saldo-disponivel.interface';

@Injectable({
  providedIn: 'root'
})
export class SaldoDisponivelService {
  private readonly API: string = `${environment.API}saldo_disponivel`;

  constructor(private http: HttpClient) { }

  getSaldoDisponivelPorUsuario(idUsuario: number): Observable<SaldoDisponivel[]>{
    return this.http.get<SaldoDisponivel[]>(`${this.API}?idUsuario=${idUsuario}`)
  }
}



