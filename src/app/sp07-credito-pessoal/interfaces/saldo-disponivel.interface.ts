export interface SaldoDisponivel {
    id: number,
    idUsuario: number,
    saldoDisponivel: number
}
