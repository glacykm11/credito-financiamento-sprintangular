import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { SaldoDisponivel } from './interfaces/saldo-disponivel.interface';
import { SaldoDisponivelService } from './services/saldo-disponivel.service';

@Component({
  selector: 'app-sp07-credito-pessoal',
  templateUrl: './sp07-credito-pessoal.component.html',
  styleUrls: ['./sp07-credito-pessoal.component.css']
})
export class Sp07CreditoPessoalComponent implements OnInit {
  saldoDisponivel: SaldoDisponivel[] = [];

  private $subs: Subscription[] = [];

  constructor(private saldoDisponivelService: SaldoDisponivelService) { }

  ngOnInit(): void {
    let idUsuario: number = 1;

    this.getSaldoDisponivelDoUsuario(idUsuario);
  }

  getSaldoDisponivelDoUsuario(idUsuario : number) : void {
    const $sub = this.saldoDisponivelService.getSaldoDisponivelPorUsuario(idUsuario).subscribe(
      (saldoDisponivel) => (this.saldoDisponivel = saldoDisponivel),
      (err) => {
        console.error(err);
      }
    );
    this.$subs.push($sub);
  }

}
