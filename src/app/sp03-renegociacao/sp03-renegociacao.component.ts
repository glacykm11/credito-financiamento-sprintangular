import { Component, OnInit } from '@angular/core';
import { getJSDocReturnTag } from 'typescript';


@Component({
  selector: 'app-sp03-renegociacao',
  templateUrl: './sp03-renegociacao.component.html',
  styleUrls: ['./sp03-renegociacao.component.css']
})
export class Sp03RenegociacaoComponent implements OnInit {
  emprestimoTotal: number = 75000

  dividaAtual: number = 15000

  qtParcelas: any = ""

  juros: number = 1.01


  today = new Date();

  mostrar: boolean = false

  mostrarTotal: boolean = false
  //acima estão as variaveis.

//abaixo estão os metodos.
  constructor() { }

  ngOnInit():void {
   
    return
  }
  


dateToString(date:Date):string{
  return date.toISOString().split('T')[0]
}
 

 daysAdder(date:Date,day:number):string{ //days adder - somador de dias.

   let miliSec= new Date(Date.parse( date.toISOString()) + day*86400000) 
   //valor referente a 1 dia, ( numeros acima são milissegundos ex: 1 dia tem 86400000 milissegundos )
   
   return miliSec.toISOString().split('T')[0];
 }

  ocultar() {
    this.mostrar = !this.mostrar 
  }
  ocultarTotal(){
    this.mostrarTotal = !this.mostrarTotal
  }

  valorParcela( ){
   let valorParcela=(this.dividaAtual/this.qtParcelas)*this.juros
   if(valorParcela!=Infinity){
      return valorParcela

   }else{
     return
   }

  }
    
}
