import { AfterViewInit, Component, Injectable, OnChanges, OnInit, ViewChild } from '@angular/core';
import { ThemePalette } from '@angular/material/core';
// import { Input } from '@material-ui/core';
// import { ngbCarouselTransitionOut } from '@ng-bootstrap/ng-bootstrap/carousel/carousel-transition';
import { PrimaryExpression } from 'typescript';

interface Select {
  value: string,
  optValue: string
}


@Component({
  selector: 'app-sp08-financiamento-ideal',
  templateUrl: './sp08-financiamento-ideal.component.html',
  styleUrls: ['./sp08-financiamento-ideal.component.css']
})
export class Sp08FinanciamentoIdealComponent{

  option:boolean = true;
  input_entrada:any;
  val_financ:any;
  val_renda:any;
  financ:number = 0;
  mostra_financ:any;
  mostra_parc:any;

  val_prest:number = 0;
  val_juros:number = 0;
  juros_pagar:number = 0;
  total_financ:number = 0;
  result_renda:string = '';
  result_entrada:string | number = 0;

  selectedObj:string = '';
  selectedParc:any;

  required:boolean = false;
  link:string = '';

  constructor() { }

  

//array para select opções de financimento
  obj_financ: Select[] = [
    {value: 'imoveis', optValue: 'Imóveis'},
    {value: 'eletronicos', optValue: 'Eletrônicos'},
    {value: 'veiculos', optValue: 'Veículos'},
    {value: 'formacao', optValue: 'Curso/Faculdade'},
    {value: 'smartphone', optValue: 'Smartphone'}
  ]
//array para select opções de parcelamento
  parcelas: Select[] = [
    {value: '6', optValue: '6x'},
    {value: '12', optValue: '12x'},
    {value: '24', optValue: '24x'},
    {value: '36', optValue: '36x'},
    {value: '48', optValue: '48x'},
    {value: '60', optValue: '60x'},
    {value: '72', optValue: '72x'},
    {value: '88', optValue: '88x'},
    {value: '96', optValue: '96x'}
  ]

  onChange(){
    this.routerLink(this.selectedObj);
  }

  ativaInput(){
    this.option = false; 
  }

  desativaInput(){
    this.option = true;  
    this.input_entrada = null;
  }

  checarValue = () =>{
    this.selectedObj == '' ? this.required = true : this.required = false;
    this.selectedParc == null ? this.required = true : this.required = false; 
  }

  routerLink = (rout:string) => {
    rout = this.selectedObj;
    switch (rout){
      case 'imoveis':
        this.link = '/credito-imobiliario';
      break;
      case 'veiculos':
        this.link = '/financimento-veiculos';
      break;
      case 'smartphone':
        this.link = '/financimento-smartphone';
      break;
      case 'eletronicos':
        this.link = '';
      break;
      case 'formacao':
        this.link = '';
      break;
    }
  }

  //verifica comprometimento da renda
  compRenda = (comp_da_renda:number) => {
      let msg:string = '';
      if (this.val_prest > comp_da_renda){
          msg = "Financimento não viável, passa de 100% da renda.";
      } else {
          msg = "Financimento de acordo com a renda.";
      }
      return msg;
  }

  validar(){
    this.checarValue();

    //verifica se input radio está diferente de vazio e faz subtração
    if(this.input_entrada != null){
      this.input_entrada = parseFloat(this.input_entrada);
      this.financ = parseFloat(this.val_financ);
      this.financ = this.financ - this.input_entrada;
    } else {
      this.financ = parseFloat(this.val_financ);
    }
    
    if (this.val_renda && this.val_financ && this.selectedParc){
      this.val_renda = parseFloat(this.val_renda);
      this.selectedParc = parseFloat(this.selectedParc);
      this.mostra_financ = this.val_financ;

      this.val_prest = this.financ / this.selectedParc;
      // função muda juros de acordo com quantidade de parcelas
      this.val_juros = this.mudarJuros(this.selectedParc);

      this.val_prest = (this.val_prest * this.val_juros) + this.val_prest;
      //verifica se renda é viável
      this.result_renda = this.compRenda(this.val_renda);

      this.juros_pagar = (this.val_prest * this.selectedParc) - this.financ;
      this.total_financ = this.financ + this.juros_pagar;
      this.val_juros = (this.val_prest / this.financ) * 100;
      
      this.input_entrada == null ? this.result_entrada = 'sem entrada' 
      : this.result_entrada = this.input_entrada; 

      this.selectedParc = this.selectedParc.toString();
      this.mostra_parc = this.selectedParc;
      
    } 
    
  }

  // mudar juros conforme option
  mudarJuros = (muda_jur:number) => {
    muda_jur = this.selectedParc;
    if(muda_jur == 6)
        muda_jur = 0.25;
    if(muda_jur == 12)
        muda_jur = 0.35;    
    if(muda_jur == 24)
        muda_jur = 0.45;
    if(muda_jur == 36)
        muda_jur = 0.50;
    if(muda_jur == 48)
        muda_jur = 0.55;
    if(muda_jur == 60)
        muda_jur = 0.65;
    if(muda_jur == 72)
        muda_jur = 0.75;
    if(muda_jur == 88)
        muda_jur = 0.85;
    if(muda_jur == 96)
        muda_jur = 0.95;    
    return muda_jur;
}

}
