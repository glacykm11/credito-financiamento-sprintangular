import { NgModule } from '@angular/core';
import { Component } from '@angular/core';
import { MatSelectModule } from '@angular/material/select'
import { MatInputModule } from '@angular/material/input';
import { CommonModule } from '@angular/common';
import { MatRadioModule } from '@angular/material/radio';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule } from '@angular/forms';
import { Sp08FinanciamentoIdealComponent } from './sp08-financiamento-ideal.component';
import { Sp08FinanciamentoIdealRoutingModule} from './sp08-financimento-ideal-routing.module';

@NgModule({
  declarations: [
    Sp08FinanciamentoIdealComponent
  ],
  imports: [
    CommonModule,
    MatSelectModule,
    MatInputModule,
    MatRadioModule,
    MatButtonModule,
    FormsModule,
    Sp08FinanciamentoIdealRoutingModule
  ]
})
export class Sp08FinanciamentoIdealModule { }