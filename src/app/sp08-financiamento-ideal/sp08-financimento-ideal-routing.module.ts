import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreditoImobiliarioComponent } from '../sp04-credito-imobiliario/credito-imobiliario.component';
import { Sp05FinanciamentoDeCarrosComponent } from '../sp05-financiamento-de-carros/sp05-financiamento-de-carros.component';
import { Sp06FinanciamentoDeSmartphoneComponent } from '../sp06-financiamento-de-smartphone/sp06-financiamento-de-smartphone.component';

const routes: Routes = [
  {
    path: 'financimento-smartphone',
    component: Sp06FinanciamentoDeSmartphoneComponent,
  },
  {
    path: 'financimento-veiculos',
    component: Sp05FinanciamentoDeCarrosComponent,
  },
  { path: 'credito-imobiliario', component: CreditoImobiliarioComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class Sp08FinanciamentoIdealRoutingModule {}
