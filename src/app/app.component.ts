import { Component } from '@angular/core';
import {
  trigger,
  transition,
  group,
  query,
  style,
  animate,
  animateChild,
} from '@angular/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('routeAnimation', [
      transition('1 => 2', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
          style({
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
          }),
        ]),
        query(':enter', [style({ left: '100%' })]), //orientação da pg que entra
        query(':leave', animateChild()),
        group([
          query(':leave', [
            animate('400ms ease-out', style({ left: '-100%' })), //orientação da pg que sai
          ]),
          query(':enter', [animate('400ms ease-out', style({ left: '0%' }))]),
        ]),
        //Primeira parte acima
        query(':enter', animateChild()),
      ]),
      transition('2 => 1', [
        style({ position: 'relative' }),
        query(':enter, :leave', [
          style({
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
          }),
        ]),
        query(':enter', [style({ left: '-100%' })]), //orientação da pg que entra
        query(':leave', animateChild()),
        group([
          query(':leave', [
            animate('400ms ease-out', style({ left: '100%' })), //orientação da pg que sai
          ]),
          query(':enter', [animate('400ms ease-out', style({ left: '0%' }))]),
        ]),
        //Primeira parte acima
        query(':enter', animateChild()),
      ]),      
    ]),
  ],
})
export class AppComponent {
  title = 'credito-financiamento';

  getDepth(outlet: any) {
    return outlet.activatedRouteData['depth'];
  }
}
