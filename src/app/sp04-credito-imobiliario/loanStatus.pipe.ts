import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'loanStatus'
})
export class LoanStatusPipe implements PipeTransform {

  transform(status: boolean | undefined): string {
    if (status == true){
      return 'Aprovado'
    }else{
      return 'Reprovado'
    }
  }

}
