export interface Solicitations {
    id: number,
    date: string,
    loanValue: number,
    loanTime: number,
    loanStatus: boolean,
    currencySalary: number,
    immobileDetails: {
        immobileType: string,
        immobileTimeBuy: number,
        immobileState: string,
        immobileCost: number,
    }
}
