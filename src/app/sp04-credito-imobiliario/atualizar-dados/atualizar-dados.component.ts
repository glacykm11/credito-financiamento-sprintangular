import { Client } from './../../../assets/interfaces/Client';
import { DataService } from './../../../assets/services/data.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HistoricoService } from '../services/historico.service';

@Component({
  selector: 'atualizar-dados',
  templateUrl: './atualizar-dados.component.html',
  styleUrls: ['./atualizar-dados.component.css'],
})
export class AtualizarDadosComponent implements OnInit {
  
  public form: FormGroup;

  client: Client = this.dataService.client;

  states: string[] = [
    'AC',
    'AL',
    'AM',
    'AP',
    'BA',
    'CE',
    'DF',
    'ES',
    'GO',
    'MA',
    'MT',
    'MS',
    'MG',
    'PA',
    'PB',
    'PR',
    'PE',
    'PI',
    'RJ',
    'RN',
    'RO',
    'RS',
    'RR',
    'SC',
    'SE',
    'SP',
    'TO',
  ];

  constructor(
    private dataService: DataService,
    private formBuilder: FormBuilder,
    private histService: HistoricoService
  ) {
    this.form = this.formBuilder.group({
      name: [this.client.clientName, Validators.required],
      account: [this.client.clientNumber],
      password: [this.client.clientPassword, Validators.required],
      email: [this.client.clientEmail, [Validators.required, Validators.email]],
      cpf: [this.client.clientCPF, Validators.required],
      rg: [this.client.clientRG, Validators.required],
      addressGroup: this.formBuilder.group({
        address: [this.client.clientAddress?.address, Validators.required],
        complement: [this.client.clientAddress?.complement],
        city: [this.client.clientAddress?.city, Validators.required],
        state: [this.client.clientAddress?.state, Validators.required],
        cep: [this.client.clientAddress?.cep, Validators.required],
      }),
      monthlyIncome: [this.client.clientMonthlyIncome, Validators.required],
    });
  }

  ngOnInit(): void {}

  isInvalidTouched(field: string): boolean | undefined {
    return this.form.get(field)?.invalid && this.form.get(field)?.touched;
  }

  applyCssError(field: string): Object {
    return {
      'ng-invalid': this.isInvalidTouched(field),
      'ng-touched:not(form)': this.isInvalidTouched(field),
    };
  }

  prepareForm(form: FormGroup) {
    return {
      //id: 0,
      date: '01 01 2021',
      loanValue: form.get('')?.value,
      details: {},
    };
  }

  onSubmit() {
    console.log(this.form);
    this.histService.create(this.prepareForm(this.form)).subscribe(
      (serverAnwser) => {
        console.log(serverAnwser);
        //this.form.reset();
      },
      (error) => alert(error),
      () => alert('complete')
    );
  }
}
