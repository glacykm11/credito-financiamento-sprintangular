import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreditoImobiliarioComponent } from './credito-imobiliario.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AtualizarDadosComponent } from './atualizar-dados/atualizar-dados.component';
import { HttpClientModule } from '@angular/common/http';
import { HistoricoComponent } from './historico/historico.component';
import { HistoricoService } from './services/historico.service';
import { IConfig, NgxMaskModule } from 'ngx-mask';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AtualizarSolicitacoesComponent } from './atualizar-solicitacoes/atualizar-solicitacoes.component';
import { LoanStatusPipe } from './loanStatus.pipe';

export const options: Partial<IConfig> | (() => Partial<IConfig>) | null = null;

@NgModule({
  declarations: [	
    CreditoImobiliarioComponent,
    AtualizarDadosComponent,
    HistoricoComponent,
    AtualizarSolicitacoesComponent,
    LoanStatusPipe
   ],
  providers: [HistoricoService],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxMaskModule.forRoot(),
    ModalModule.forRoot()
  ]
})

export class ImobiliarioModule { }
