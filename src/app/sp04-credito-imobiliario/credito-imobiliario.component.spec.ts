import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditoImobiliarioComponent } from './credito-imobiliario.component';

describe('CreditoImobiliarioComponent', () => {
  let component: CreditoImobiliarioComponent;
  let fixture: ComponentFixture<CreditoImobiliarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreditoImobiliarioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditoImobiliarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
