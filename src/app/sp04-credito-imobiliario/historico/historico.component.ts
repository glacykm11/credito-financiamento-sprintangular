import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Solicitations } from '../interfaces/solicitations';
import { HistoricoService } from '../services/historico.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'historico',
  templateUrl: './historico.component.html',
  styleUrls: ['./historico.component.css'],
})
export class HistoricoComponent implements OnInit {

  solicitation?: Solicitations;

  solicitations$?: Observable<Solicitations[]>;

  modalRef: BsModalRef = new BsModalRef();

  message: string = '';

  @ViewChild('detelePopUp') detelePopUp?: TemplateRef<any>;

  @ViewChild('editPopUp') editPopUp?: TemplateRef<any>;

  constructor(
    private histService: HistoricoService,
    private modalService: BsModalService
  ) {}

  ngOnInit() {
    //this.histService.list().subscribe(solicitations => this.solicitations = solicitations)
    this.onRefresh();
  }

  onRefresh(): Observable<Solicitations[]> {
    return (this.solicitations$ = this.histService.list());
  }

  onDelete(solicitation: Solicitations) {
    this.solicitation = solicitation;
    if (this.detelePopUp) {
      this.modalRef = this.modalService.show(this.detelePopUp, {
        class: 'modal-sm',
      });
    }
  }

  onEdit(solicitation: Solicitations) {
    this.histService.emitSolicitation(solicitation);
    this.solicitation = solicitation;
    if (this.editPopUp) {
      this.modalRef = this.modalService.show(this.editPopUp, {
        class: 'modal-lg',
      });
    }
  }

  confirmDelete(): void {
    console.log(this.solicitation?.id);
    this.histService.delete(this.solicitation?.id).subscribe(
      (success) => this.onRefresh(),
      (error) => console.log,
      () => console.log
    );
    this.modalRef.hide();
  }

  declineDelete(): void {
    this.modalRef.hide();
  }
}
