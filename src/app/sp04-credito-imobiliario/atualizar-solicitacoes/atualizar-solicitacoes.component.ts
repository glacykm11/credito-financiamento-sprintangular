import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Solicitations } from '../interfaces/solicitations';
import { HistoricoService } from '../services/historico.service';

@Component({
  selector: 'atualizar-solicitacoes',
  templateUrl: './atualizar-solicitacoes.component.html',
  styleUrls: ['./atualizar-solicitacoes.component.css'],
})
export class AtualizarSolicitacoesComponent implements OnInit {

  public form: FormGroup;

  states: string[] = [
    'AC',
    'AL',
    'AM',
    'AP',
    'BA',
    'CE',
    'DF',
    'ES',
    'GO',
    'MA',
    'MT',
    'MS',
    'MG',
    'PA',
    'PB',
    'PR',
    'PE',
    'PI',
    'RJ',
    'RN',
    'RO',
    'RS',
    'RR',
    'SC',
    'SE',
    'SP',
    'TO',
  ];

  solicitation?: Solicitations;

  selectedState?: string;

  showNegate: boolean = true;

  showSuccess: boolean = true;

  constructor(
    private formBuilder: FormBuilder,
    private histService: HistoricoService
  ) {
    histService.solicitation.subscribe(
      (solicitation) => (this.solicitation = solicitation)
    );

    this.form = this.formBuilder.group({
      immobileType: [this.solicitation?.immobileDetails.immobileType],
      immobileBuyTime: [this.solicitation?.immobileDetails.immobileTimeBuy],
      immobileState: [this.solicitation?.immobileDetails.immobileState],
      immobileValue: [this.solicitation?.immobileDetails.immobileCost],
      loanValue: [this.solicitation?.loanValue],
      monthlyIncome: [this.solicitation?.currencySalary],
      loanTime: [this.solicitation?.loanTime],
      loanStatus: [this.solicitation?.loanStatus]
    });

    this.selectedState = this.solicitation?.immobileDetails.immobileState

    console.log(this.selectedState)
  }

  ngOnInit() {
    //this.histService.emitterSolicitation.subscribe(oi => this.solicitation = oi)
    //console.log(this.solicitation)
  }

  prepareForm() {
    let date: Date = new Date(); //Retorna data atual
    return {
      date: `${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()}`,
      loanValue: this.form.get('loanValue')?.value,
      loanTime: this.form.get('loanTime')?.value,
      loanStatus: this.analyse(),
      currencySalary: this.form.get('monthlyIncome')?.value,
      immobileDetails: {
        immobileType: this.form.get('immobileType')?.value,
        immobileTimeBuy: this.form.get('immobileBuyTime')?.value, 
        immobileState: this.form.get('immobileState')?.value,
        immobileCost: this.form.get('immobileValue')?.value,
      },
    };
  }

  analyse(): boolean {

    this.showSuccess = true;
    this.showNegate = true;

    if(parseFloat(this.form.get('immobileValue')?.value)/parseFloat(this.form.get('loanValue')?.value) >= 1){
      this.showSuccess = !this.showSuccess;
      return true
    } else {
      this.showNegate = !this.showNegate;
      return false
    }
  }

  onSubmit() {
    this.histService.edit(this.solicitation?.id, this.prepareForm()).subscribe(
      (success) => {
        console.log('success');
      },
      (error) => console.log(error),
      () => console.log('complete')
    );
  }


}
