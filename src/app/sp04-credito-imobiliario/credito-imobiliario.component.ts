import { Client } from './../../assets/interfaces/Client';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HistoricoService } from './services/historico.service';
import { DataService } from 'src/assets/services/data.service';

@Component({
  selector: 'credito-imobiliario',
  templateUrl: 'credito-imobiliario.component.html',
  styleUrls: ['credito-imobiliario.component.css'],
})
export class CreditoImobiliarioComponent implements OnInit {
  client: Client;

  states: string[] = [
    'AC',
    'AL',
    'AM',
    'AP',
    'BA',
    'CE',
    'DF',
    'ES',
    'GO',
    'MA',
    'MT',
    'MS',
    'MG',
    'PA',
    'PB',
    'PR',
    'PE',
    'PI',
    'RJ',
    'RN',
    'RO',
    'RS',
    'RR',
    'SC',
    'SE',
    'SP',
    'TO',
  ];

  showNegate: boolean = true;

  showSuccess: boolean = true;

  showReviewData: boolean = true;

  constructor(
    private dataService: DataService,
    private histService: HistoricoService
  ) {
    this.client = dataService.client;
  }

  ngOnInit() {}

  prepareForm(form: NgForm) {
    let date: Date = new Date(); //Retorna data atual
    return {
      date: `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`,
      loanValue: form.value.valorFinanciamento,
      loanTime: form.value.prazoFinanciamento,
      loanStatus: this.analyse(form),
      currencySalary: this.client.clientMonthlyIncome,
      immobileDetails: {
        immobileType: form.value.tipoDeImovel,
        immobileTimeBuy: form.value.tempoCompraImovel,
        immobileState: form.value.localizacaoImovel,
        immobileCost: form.value.valorImovel,
      },
    };
  }

  onSubmit(form: NgForm) {
    this.histService.create(this.prepareForm(form)).subscribe(
      (success) => {
        console.log('success');
      },
      (error) => console.log(error),
      () => console.log('complete')
    );
  }

  reviewData() {
    this.showReviewData = !this.showReviewData;
  }

  analyse(form: NgForm): boolean {
    this.showSuccess = true;
    this.showNegate = true;

    if (
      parseFloat(form.value.valorImovel) /
        parseFloat(form.value.valorFinanciamento) >=
      1
    ) {
      this.showSuccess = !this.showSuccess;
      return true;
    } else {
      this.showNegate = !this.showNegate;
      return false;
    }
  }
}
