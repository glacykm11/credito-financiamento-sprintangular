import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { BehaviorSubject, from, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

import { Solicitations } from '../interfaces/solicitations';


@Injectable({
  providedIn: 'root'
})
export class HistoricoService {

  private readonly API: string = `${environment.API}solicitations`

  _solicitation!: Solicitations;

  solicitation = new BehaviorSubject<Solicitations>(this._solicitation);

  constructor(private http: HttpClient) { }

  list(): Observable<Solicitations[]>{
    //Para que um observable envolva a saída do método http, utilizei o casting <Solicitacoes[]> 
    return this.http.get<Solicitations[]>(this.API)
  }

  create(solicitation: any): Observable<Solicitations[]>{
    return this.http.post<Solicitations[]>(this.API, solicitation)
  }

  delete(solicitationId: any): Observable<any>{
    return this.http.delete(`${this.API}/${solicitationId}`, solicitationId)
  }

  edit(solicitationId: any, solicitation: any): Observable<any>{
    return this.http.put(`${this.API}/${solicitationId}`, solicitation)
  }

  emitSolicitation(solicitation: Solicitations){
    this.solicitation.next(solicitation);
  }

}



